﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Gift
    {
        private string nameGift;
        public string name { get { return nameGift; } set { nameGift = value; } }

        private double weightGigt;

        private bool packingGift;

        private string forString;
        public bool packing { get { return packingGift; } set { packingGift = value; } }

        public Gift(string name, bool packing)
        {
            nameGift = name;
            packingGift = packing;
        }

        protected List<Candy> listCandies = new List<Candy>();

        public void addCandy(Candy candy)
        {
            listCandies.Add(candy);
        }

        public void removeCandy(int i)
        {
            listCandies.Remove(listCandies[i]);
        }

        public List<Candy> getCandy()
        {
            return listCandies;
        }

        public string totalVvod()
        {

            for (int i = 0; i < listCandies.Count(); i++)
            {
                Candy c = listCandies[i];
                forString += "конфету " + c.name + " c весом " + c.calculateWeight() + "г, ";
            }
            if (packingGift == true)
                forString += " присутствует упаковка." + "\n";
            else
                forString += " упаковка отсуствует" + "\n";

            return forString;
        }
        public double totalWeight()
        {
            weightGigt = 0;
            for (int i = 0; i < listCandies.Count(); i++)
            {
                weightGigt += listCandies[i].calculateWeight();
            }

            if (packingGift == true)
            {
                weightGigt += 15;
            }

            return weightGigt;
        }
    }
}
