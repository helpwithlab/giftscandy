﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class WaffleCandy : Candy
    {
        private double wWaffle;
        private double wFilling;
        public double weightWaffle { get { return wWaffle; } set { wWaffle = value; } }
        public double weightFilling { get { return wFilling; } set { wFilling = value; } }

        public WaffleCandy(string name, string producer, double weightW, double weightF) : base(name, producer)
        {
            wWaffle = weightW;
            wFilling = weightF;
        }

        public override double calculateWeight()
        {
            weight = wWaffle + wFilling;
            return weight;
        }
    }
}
