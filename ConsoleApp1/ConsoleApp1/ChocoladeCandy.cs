﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class ChocoladeCandy : Candy
    {
        private double cacaoCandy;
        public double cacao { get { return cacaoCandy; } set { cacaoCandy = value; } }

        private double wFilling;
        public double weightFilling { get { return wFilling; } set { wFilling = value; } }


        public override double calculateWeight()
        {
            weight = 15;

            if (cacaoCandy > 80)
            {
                weight += 5;
            }

            else
            {
                if (cacao > 50)
                {
                    weight += 3;
                }

            }

            if (wFilling > 0)
            {
                weight += wFilling;
            }
            return weight;

        }

        public ChocoladeCandy(string name, string producer, double cacaoC, double weight) : base(name, producer)
        {
            cacaoCandy = cacaoC;
            wFilling = weight;
        }
    }
}
