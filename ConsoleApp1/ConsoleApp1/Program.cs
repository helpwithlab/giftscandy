﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Gift one = new Gift("Зимушка", true);
            Gift two = new Gift("Подарок от Морозки", true);
            Gift three = new Gift("Новогодний", false);

            ChocoladeCandy c1 = new ChocoladeCandy("Метелица", "Красная звезда", 0.75, 12);
            ChocoladeCandy c2 = new ChocoladeCandy("Марс", "Сентябрь", 0.9, 15);
            ChocoladeCandy c3 = new ChocoladeCandy("Сникерс", "Антонов Двор", 0.35, 10);
            ChocoladeCandy c4 = new ChocoladeCandy("Спринт", "Любятово", 0.50, 11);
            ChocoladeCandy c5 = new ChocoladeCandy("Маска", "Яшкино", 0.6, 14);

            MarmaladeCandy m1 = new MarmaladeCandy("Лучик", "Яшкино", true);
            MarmaladeCandy m2 = new MarmaladeCandy("Вкуснятина", "Любятово", true);
            MarmaladeCandy m3 = new MarmaladeCandy("Сластена", "Антонов Двор", false);
            MarmaladeCandy m4 = new MarmaladeCandy("Сладко", "Сентябрь", false);
            MarmaladeCandy m5 = new MarmaladeCandy("Малиновый Рай", "Красная звезда", true);

            WaffleCandy w1 = new WaffleCandy("Вафелька", "Красная звезда", 6, 5);
            WaffleCandy w2 = new WaffleCandy("Хрустик", "Антонов Двор", 4, 5.2);
            WaffleCandy w3 = new WaffleCandy("Наслаждение", "Яшкино", 6, 4);
            WaffleCandy w4 = new WaffleCandy("Вафля", "Любятово", 4.3, 5.7);
            WaffleCandy w5 = new WaffleCandy("Клеточка", "Сентябрь", 6.5, 6);

            one.addCandy(c1);
            one.addCandy(c2);
            one.addCandy(m1);
            one.addCandy(w4);
            one.addCandy(w5);

            two.addCandy(c3);
            two.addCandy(m2);
            two.addCandy(m3);
            two.addCandy(w3);
            two.addCandy(w2);

            three.addCandy(c4);
            three.addCandy(c5);
            three.addCandy(m4);
            three.addCandy(m5);
            three.addCandy(w1);

            Console.WriteLine("Подарок: " + one.name + " весит: " + one.totalWeight());
            Console.WriteLine("Содержит: " + one.totalVvod());

            Console.WriteLine("Подарок: " + two.name + " весит: " + two.totalWeight());
            Console.WriteLine("Содержит: " + two.totalVvod());

            Console.WriteLine("Подарок: " + three.name + " весит: " + three.totalWeight());
            Console.WriteLine("Содержит: " + three.totalVvod());
            Console.ReadKey();
        }
    }
}
