﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class MarmaladeCandy : Candy
    {
        private bool glazeCandy;
        public bool glaze { get { return glazeCandy; } set { glazeCandy = value; } }

        public MarmaladeCandy(string name, string producer, bool glaze) : base(name, producer)
        {
            glazeCandy = glaze;
        }


        public override double calculateWeight()
        {
            weight = 11;

            if (glazeCandy == true)
            {
                weight += 2;
            }

            return weight;
        }
    }
}
