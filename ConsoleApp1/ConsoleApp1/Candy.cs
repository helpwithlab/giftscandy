﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public abstract class Candy
    {
        private string nameCandy;
        public string name { get { return nameCandy; } set { nameCandy = value; } }
        private string producerCandy;
        public string producer { get { return producerCandy; } set { producerCandy = value; } }
        protected double weight;

        public Candy(string name, string producer)
        {
            nameCandy = name;
            producerCandy = producer;
        }
        public abstract double calculateWeight();

        public override string ToString()
        {
            return this.nameCandy + ", производитель: " + this.producerCandy + " " + this.calculateWeight() + "г";
        }
    }
}
