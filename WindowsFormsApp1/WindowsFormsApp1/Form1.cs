﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConsoleApp1;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listCandies.SelectedIndex>=0)
            {
                Creator.g.removeCandy(listCandies.SelectedIndex);
                Creator.form.listCandies.DataSource = Creator.g.getCandy().ToList();
                label2.Text = "Общий вес подарка: " + Convert.ToString(Creator.g.totalWeight()) + "г";
            }
        }

        private void change_Click(object sender, EventArgs e)
        {
            FormForChange CF = new FormForChange();
            CF.Show();
            this.Hide();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            FormForAdd AF = new FormForAdd();
            AF.Show();
            this.Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Creator.form.listCandies.DataSource = Creator.g.getCandy().ToList();
        }
    }
}
