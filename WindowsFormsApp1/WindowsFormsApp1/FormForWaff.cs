﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConsoleApp1;

namespace WindowsFormsApp1
{
    public partial class FormForWaff : Form
    {
        public FormForWaff()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            Creator.form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "")
            {
                WaffleCandy w = new WaffleCandy(Convert.ToString(textBox3.Text), Convert.ToString(textBox4.Text), Convert.ToInt32(textBox2.Text), Convert.ToInt32(textBox1.Text));
                Creator.g.addCandy(w);
                Creator.form.listCandies.DataSource = Creator.g.getCandy().ToList();
                Creator.form.label2.Text = "Итоговый вес подарка: " + Convert.ToString(Creator.g.totalWeight()) + " г";
                Close();
                Creator.form.Show();
            }

            else { MessageBox.Show("Пожалуйста, заполните все поля"); }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
                e.Handled = true;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
                e.Handled = true;
        }
    }
}
