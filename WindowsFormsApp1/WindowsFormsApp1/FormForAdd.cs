﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConsoleApp1;

namespace WindowsFormsApp1
{
    public partial class FormForAdd : Form
    {
        public FormForAdd()
        {
            InitializeComponent();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Close();
            Creator.form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    FormForChoc2 CF = new FormForChoc2();
                    CF.Show();
                    break;

                case 1:
                    FormForMarm MF = new FormForMarm();
                    MF.Show();
                    break;

                case 2:
                    FormForWaff WF = new FormForWaff();
                    WF.Show();
                    break;
            }
            Close();
        }
    }
}
