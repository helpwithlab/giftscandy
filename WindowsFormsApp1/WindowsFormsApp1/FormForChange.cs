﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConsoleApp1;

namespace WindowsFormsApp1
{
    public partial class FormForChange : Form
    {
        public FormForChange()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            Creator.form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                Creator.g.getCandy()[Creator.form.listCandies.SelectedIndex].name = textBox1.Text;
                Creator.g.getCandy()[Creator.form.listCandies.SelectedIndex].producer = textBox2.Text;
                Creator.form.listCandies.DataSource = Creator.g.getCandy().ToList();
                Creator.form.label2.Text = "Итоговый вес подарка: " + Convert.ToString(Creator.g.totalWeight()) + " г";
                Close();
                Creator.form.Show();

            }
            else { MessageBox.Show("Пожалуйста, заполните все поля"); }
        }

        private void FormForChange_Load(object sender, EventArgs e)
        {
            string name = Creator.g.getCandy()[Creator.form.listCandies.SelectedIndex].name;
            string producer = Creator.g.getCandy()[Creator.form.listCandies.SelectedIndex].producer;
            textBox1.Text = Convert.ToString(name);
            textBox2.Text = Convert.ToString(producer);
        }
    }
}
